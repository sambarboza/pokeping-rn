import React, { Component } from 'react';
import {
  Alert,
  ActivityIndicator,
  AppState,
  AsyncStorage,
  BackAndroid,
  Button,
  Dimensions,
  Image,
  Modal,
  Platform,
  PermissionsAndroid,
  StyleSheet,
  Text,
  TouchableHighlight,
  View
} from 'react-native'
import MapView from 'react-native-maps'
import mapStyle from './mapStyle'
import { DigitsManager } from 'react-native-fabric-digits'
import Register from './Register'
import GymDetails from './GymDetails'
import UpdateGym from './UpdateGym'
var Fabric = require('react-native-fabric');
var { Answers, Crashlytics } = Fabric;
import { AdMobBanner } from 'react-native-admob'

const neutralPin = require('./assets/neutral_pin.png');
const valorPin = require('./assets/valor_pin.png');
const mysticPin = require('./assets/mystic_pin.png');
const instinctPin = require('./assets/instinct_pin.png');

export default class PokePing extends Component {

  constructor(props) {
    super(props);
    this.state = {
      showRegistrationModal: false,
      userLocation: {
        latitude: 34.0103148,
        longitude: -118.4962526
      },
      loading: false,
      gyms: [],
      userId: null,
      digitsUserId: null,
      //selectedGym: {name:'Fuente Mall El Tesoro', _id: '098123dslj'},
      //gymToUpdate: '098123dslj'
    }
    this.digitsCompletion = this.digitsCompletion.bind(this);
  }

  componentDidMount() {
    AppState.addEventListener('change', this._handleAppStateChange);
    this.checkDigitsSession()
    Answers.logContentView('Map', 'Screen', '')
    AsyncStorage.getItem('userId', (err, userId) => {
      if (!err) { this.setState({userId}) }
    })
    if (Platform.OS === 'android') {
      PermissionsAndroid.checkPermission(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      ).then((granted) => {
        if (!granted) {
          this.requestCameraPermission()
        } else {
          this.getUserLocation()
        }
      })
    } else {
      this.getUserLocation()
    }
    BackAndroid.addEventListener('hardwareBackPress', () => {
      if (this.state.selectedGym) {
        console.log('CLose Gym', arguments)
        this.closeGymDetails()
        return true
      }
    });
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (appState) => {
    if (appState === 'active') {
      this.checkDigitsSession()
      if (this.state.mapRegion) {
        this.loadGyms(this.state.mapRegion)
      }
    }
  };

  checkDigitsSession() {
    DigitsManager.sessionDetails((error, sessionDetails) => {
      if (!error && sessionDetails) {
        const digitsUserId = sessionDetails.userId || sessionDetails.userID
        this.findUser(digitsUserId)
      }
    })
  }

  requestCameraPermission() {
    Answers.logCustom('Request Camera Permission')
    try {
      PermissionsAndroid.requestPermission(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
      ).then((granted) => {
        if (granted) {
          Answers.logCustom('Camera Permission Granted')
          this.getUserLocation()
        } else {
          Answers.logCustom('Camera Permission Denied')
          this.requestCameraPermission()
        }
      }).catch((e) => {
        this.requestCameraPermission()
        console.warn(e)
      })
    } catch (err) {
      Answers.logCustom('Request Camera Permission Error', err)
      console.warn(err)
    }
  }

  getUserLocation() {
    const map = this.refs.map
    Answers.logCustom('Get User Location')
    navigator.geolocation.getCurrentPosition((position) => {
      const userLocation = {
        latitude: position.coords.latitude,
        longitude: position.coords.longitude,
      }
      this.setState({userLocation})
      map.animateToCoordinate(userLocation, 700)
    }, (error) => alert(JSON.stringify(error)),
      {enableHighAccuracy: true, timeout: 20000}
    )
  }

  loginWithDigits() {
    Answers.logCustom('Login With Digits')
    this.setState({selectedGym: null, gymToUpdate: null})
    DigitsManager.launchAuthentication({}).then((responseData) => {
      Answers.logCustom('Login With Digits Success', responseData)
      console.log("[Digits] Login Successful", responseData);
      this.digitsCompletion(responseData);
    }).catch((error) => {
      Answers.logCustom('Login With Digits Error', {error: error.message})
      if (error && error.code != 1) {
        console.error("[Digits] Login Error", JSON.stringify(error));
      }

    });
  }

  digitsCompletion(response) {
    this.setState({loading: true})
    DigitsManager.sessionDetails((error, sessionDetails) => {
      console.log('DigitsSessionDetails', error, sessionDetails)
      let userId = null
      if (error) {
        console.log(error)
      } else if (sessionDetails) {
        const userId = sessionDetails.userId || sessionDetails.userID
        this.findUser(userId)
      }
      this.setState({loading: false})
    })
  }

  loadDigitsSession() {
    DigitsManager.sessionDetails((error, sessionDetails) => {
      if (!error && sessionDetails) {
        const digitsUserId = sessionDetails.userId || sessionDetails.userID
        this.setState({showRegistrationModal: true, digitsUserId})
      } else {
        this.loginWithDigits()
      }
    })
  }

  /**
  * Check if user exists, save it's id to local Storage if so or
  * show the registration modal if don't.
  */
  findUser(digitsUserId) {
    Answers.logCustom('Find User')
    console.log('Find User', digitsUserId)
    this.setState({loading: true})
    fetch(`http://104.131.15.66/user/${digitsUserId}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((res) => {
      console.log('Res of GetUser', JSON.stringify(res))
      this.setState({loading: false})
      if (res.ok) {
        Answers.logCustom('Find User Success')
        res.json().then(res => {
          console.log('User', res.user)
          console.log('UserId', res.user._id)
          AsyncStorage.setItem('userId', res.user._id)
          this.setState({
            showRegistrationModal: false,
            digitsUserId,
            loading: false,
            userId: res.user._id,
            gymToUpdate: this.state.gymToUpdateAfterLogin,
            gymToUpdateAfterLogin: null
          })
        })
      } else {
        Answers.logCustom('Find User Error')
        console.log('Res NotOk')
        this.loadDigitsSession()
      }
    }).catch((error) => {
      Answers.logCustom('Find User Error', {error: error.message})
      console.log('Find User Error', error.message);
      this.setState({showRegistrationModal: true, digitsUserId})
    })
  }

  /**
  * Load Gyms using region (camera viewport) to get LatLng bounds.
  */
  loadGyms(region) {
    Answers.logCustom('Load Gyms')
    this.setState({loading: true})
    let self = this
    const fromLat = region.latitude - region.latitudeDelta
    const toLat = region.latitude + region.latitudeDelta
    const fromLng = region.longitude - region.longitudeDelta
    const toLng = region.longitude + region.longitudeDelta

    let params = `fromLat=${fromLat}&toLat=${toLat}&fromLng=${fromLng}&toLng=${toLng}`
    fetch(`http://104.131.15.66/gyms?${params}`, {
      method: 'GET',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }).then((response) => response.json()).then((gyms) => {
      Answers.logCustom('Load Gyms Success')
      self.showGyms(gyms)
    }).catch((error) => {
      Answers.logCustom('Load Gyms Error', error)
      console.error(error);
    })
  }

  showGyms(gyms) {
    Answers.logContentView('Gyms', 'MapItem', '', {amount: gyms.length})
    this.setState({gyms, loading: false})
  }

  centerOnUserLocation() {
    if (this.state.userLocation) {
      this.refs.map.animateToCoordinate(this.state.userLocation, 700)
    } else {
      this.getUserLocation()
    }
  }

  getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = this.deg2rad(lat2-lat1);  // deg2rad below
    var dLon = this.deg2rad(lon2-lon1);
    var a =
      Math.sin(dLat/2) * Math.sin(dLat/2) +
      Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
      Math.sin(dLon/2) * Math.sin(dLon/2)
      ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d;
  }

  deg2rad(deg) {
    return deg * (Math.PI/180)
  }

  /**
  * Called when map camera changes.
  */
  onRegionChange(mapRegion) {
    Answers.logCustom('Map Region Changed')

    let changeDistanceTreshold = true
    //Check change distance from the last mapRegion
    if (this.state.mapRegion) {
      lat1 = this.state.mapRegion.latitude
      lng1 = this.state.mapRegion.longitude
      lat2 = mapRegion.latitude
      lng2 = mapRegion.longitude
      const changeDistance = this.getDistanceFromLatLonInKm(lat1, lng1, lat2, lng2)
      if (changeDistance < 5) {
        changeDistanceTreshold = false
      }
    }

    this.setState({mapRegion})
    clearTimeout(this.regionChangeTimeoutBounce)
    this.regionChangeTimeoutBounce = setTimeout(() => {
      if (!this.state.loading && !this.state.selectedGym && changeDistanceTreshold) {
        Answers.logSearch('Gyms', mapRegion)
        this.loadGyms(mapRegion)
      }
    }, 1300)
  }

  /**
  * Load Gym Details when clicking in a Gym.
  */
  onGymMarkerClicked(selectedGym) {
    Answers.logCustom('Gym Marker Clicked', selectedGym)
    this.setState({selectedGym})
    this.refs.map.animateToRegion({
      latitude: selectedGym.location.coordinates[1],
      longitude: selectedGym.location.coordinates[0],
      latitudeDelta: 0.01844,
      longitudeDelta: 0.01684,
    }, 400)
  }

  closeGymDetails() {
    this.setState({selectedGym: null})
  }

  showLoginDialog() {
    DigitsManager.sessionDetails((error, sessionDetails) => {
      if (sessionDetails && (sessionDetails.userId || sessionDetails.userID)) {
        const digitsUserId = sessionDetails.userId || sessionDetails.userID
        this.findUser(digitsUserId)
      } else {
        Answers.logCustom('Login Request')
        Alert.alert('Please login to continue', '', [
          {text: 'Ok', onPress: () => this.loginWithDigits() }
        ])
      }
    })
  }

  onUpdateGymClicked(gymId) {
    Answers.logCustom('Update Gym Clicked', {gymId})
    //TODO remove duplicated logic in GymDetails
    if (!this.state.userId) {
      this.showLoginDialog()
      this.setState({gymToUpdateAfterLogin: gymId})
    } else {
      this.setState({selectedGym: null, gymToUpdate: gymId})
    }
  }

  getMarker(gym) {
    const pin = this.getTeamPin(gym.team)
    return <MapView.Marker key={gym.pokeId}
      image={pin}
      onPress={this.onGymMarkerClicked.bind(this, gym)}
      coordinate={{
        latitude: gym.location.coordinates[1],
        longitude: gym.location.coordinates[0]
      }} />
  }

  getTeamPin(team) {
    switch (team) {
      case 0:
        return valorPin
      case 1:
        return mysticPin
      case 2:
        return instinctPin
      default:
        return neutralPin
    }
  }

  requestLoginToVote(gym) {
    this.setState({selectedGym: gym, gymToUpdateAfterLogin: null})
    this.showLoginDialog()
  }

  bannerError() {
    console.log('Banner Error', arguments)
    Answers.logCustom('Failed to load banner')
  }

  render() {
    const { height, width } = Dimensions.get('window')
    const gymDetailsMarginTop = (height / 2) - 100
    const error = this.state.error ? <Text>An error occured.</Text> : null;
    let selectedGym = (this.state.selectedGym) ?
      <GymDetails gym={this.state.selectedGym}
        userId={this.state.userId}
        requestLogin={this.requestLoginToVote.bind(this)}
        onUpdateGymClicked={this.onUpdateGymClicked.bind(this)}
        style={{marginTop: gymDetailsMarginTop}}/>
      : null

    return (
      <View style={styles.container}>
        <View style={styles.mapContainer}>
          <MapView style={styles.map} ref="map"
            onPress={this.closeGymDetails.bind(this)}
            showsMyLocationButton={true}
            customMapStyle={mapStyle}
            provider={(Platform.OS === 'android') ? MapView.PROVIDER_GOOGLE : null}
            onRegionChangeComplete={this.onRegionChange.bind(this)}
            initialRegion={{
             latitude: this.state.userLocation.latitude,
             longitude: this.state.userLocation.longitude,
             latitudeDelta: 0.0922,
             longitudeDelta: 0.0421
          }}>
            <MapView.Marker
              anchor={{x: 0.5, y: 0.5}}
              coordinate={this.state.userLocation}
              image={require('./assets/userpin.png')}
              title="You are here"/>
            {this.state.gyms.map(gym => (
              this.getMarker(gym)
            ))}
          </MapView>

          {error}
          <ActivityIndicator
            animating={this.state.loading}
            style={[{height: 80}]}
            size="large"/>

          {(this.state.showRegistrationModal) ?
            <View style={styles.container}>
              <Modal
                animationType={"slide"}
                transparent={false}
                visible={true}
                onRequestClose={() => {
                  this.setState({showRegistrationModal: false})
                }}>
                <Register
                  close={() => {
                    this.setState({showRegistrationModal: false})
                  }}
                  userId={this.state.digitsUserId} />
              </Modal>
            </View> : null
          }

          {(this.state.gymToUpdate) ?
            <View style={styles.container}>
              <Modal animationType={"slide"} transparent={false} visible={true}
                onRequestClose={() => {
                  this.setState({gymToUpdate: null, selectedGym: null})
                }}>
                <UpdateGym close={() => {
                    this.setState({gymToUpdate: null, selectedGym: null})
                    this.loadGyms(this.state.mapRegion)
                  }}
                  userId={this.state.userId}
                  gymId={this.state.gymToUpdate} />
              </Modal>
            </View> : null
          }

          <TouchableHighlight style={styles.myLocation}
            onPress={this.centerOnUserLocation.bind(this)}>
            <Image source={require('./assets/my_location.png')} />
          </TouchableHighlight>

          {selectedGym}

        </View>
        <AdMobBanner
          adUnitID="ca-app-pub-8358930606418017/3216687687"
          testDeviceID="EMULATOR"
          didFailToReceiveAdWithError={this.bannerError} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mapContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  myLocation: {
    bottom: 16,
    right: 16,
    position: 'absolute',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
