import React, { Component } from 'react';
import {
  ActivityIndicator,
  Alert,
  Animated,
  AsyncStorage,
  Dimensions,
  Easing,
  Image,
  Modal,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';
import Button from './Button';
var Fabric = require('react-native-fabric');
var entities = require("entities");
var { Answers, Crashlytics } = Fabric;

export default class GymDetails extends Component {
  totalPrestige = [0, 2, 4, 8, 12, 16, 20, 30, 40, 50, 52];

  constructor(props) {
    super(props)
    let score = (props.gym.score) ? props.gym.score : 0
    this.state = { score }
  }

  componentDidMount() {
    const updates = (this.props.gym.updates) ? this.props.gym.updates.length : 0
    Answers.logContentView('Gym Detail', 'Screen', '', {
      name: this.props.gym.name,
      id: this.props.gym._id,
      team: this.props.gym.team,
      level: this.props.gym.level,
      prestige: this.props.gym.prestige,
      updates
    })
  }

  componentWillReceiveProps(props) {
    let score = 0
    const updates = (props.gym.updates) ? props.gym.updates.length : 0
    if (updates && updates.length > 0) {
      update = updates[updates.length-1]
      score = update.score
    }
    this.setState({score})
  }

  getTeamIcon() {
    switch (this.props.gym.team) {
      case 0:
        return <Image style={styles.teamIcon} resizeMode={'contain'}
          source={require('./assets/ic_valor.png')} />
      case 1:
        return <Image style={styles.teamIcon} resizeMode={'contain'}
          source={require('./assets/ic_mystic.png')} />
      case 2:
        return <Image style={styles.teamIcon} resizeMode={'contain'}
          source={require('./assets/ic_instinct.png')} />
      default:
        return <Image style={styles.teamIcon} resizeMode={'contain'}
          source={require('./assets/ic_no_team.png')} />
    }
  }

  getTeamColor(team) {
    switch (team) {
      case 0:
        return '#F3150A'
      case 1:
        return '#0677EE'
      case 2:
        return '#FFD303'
      default:
        return '#F1F1F1'
    }
  }

  getMaxPrestige(level) {
    const maxPrestige = this.totalPrestige[level] * 1000
    return maxPrestige
  }

  getPrestigeText(prestige, level) {
    const maxPrestige = this.getMaxPrestige(level)
    return `${prestige}/${maxPrestige}`
  }

  timeSince(date) {
    date = new Date(date)
    var seconds = Math.floor((new Date() - date) / 1000);
    var interval = Math.floor(seconds / 31536000);

    if (interval > 1) {
        return interval + " years";
    }
    interval = Math.floor(seconds / 2592000);
    if (interval > 1) {
        return interval + " months";
    }
    interval = Math.floor(seconds / 86400);
    if (interval > 1) {
        return interval + " days";
    }
    interval = Math.floor(seconds / 3600);
    if (interval > 1) {
        return interval + " hours";
    }
    interval = Math.floor(seconds / 60);
    if (interval > 1) {
        return interval + " minutes";
    }
    return Math.floor(seconds) + " seconds";
  }

  getUpdatedAtText(update) {
    return `Last updated ${this.timeSince(update.date)} ago by ${update.user.nickname}`
  }

  vote(update, upvote) {
    if (!this.props.userId) {
      this.props.requestLogin(this.props.gym)
      return
    }

    Answers.logCustom('Vote Request Sent')
    const vote = { upvote, user: this.props.userId }
    fetch(`http://104.131.15.66/gyms/vote/${update._id}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(vote)
    }).then((res) => {
      if (res.ok) {
        Answers.logCustom('Vote Request Success')
        console.log('Voted Successfully')
        let score = (upvote) ? this.state.score + 1 : this.state.score - 1
        this.setState({score})
      } else {
        Answers.logCustom('Vote Request Error', {error: res._bodyText})
        console.log('Vote Request Error', res._bodyText, JSON.stringify(res))
      }
    }).catch((error) => {
      Answers.logCustom('Vote Request Error', {error})
      console.log('Error', error);
    })
  }

  render() {
    let { name, team, level, prestige, updates, reporter, _id } = this.props.gym
    name = entities.decodeHTML(name)
    let update
    if (updates && updates.length > 0) {
      update = updates[updates.length-1]
    }

    const teamColor = this.getTeamColor(team)
    const levelText = (level != null) ? level : '?'

    return <View style={[styles.container, this.props.style]}>
      <View style={styles.triangleContainer}>
        <Image style={[styles.triangle, {tintColor: teamColor}]}
          source={require('./assets/triangle.png')} />
      </View>
      <View style={[styles.line, {backgroundColor: teamColor}]} />
      <View style={styles.content}>

        <View style={styles.header}>
          {this.getTeamIcon()}
          <View style={styles.namePrestigeContainer}>
            <Text style={styles.name}>{name}</Text>
            {(prestige) ?
            <Text style={[styles.prestige, {color: teamColor}]}>
              {this.getPrestigeText(prestige, level)}
            </Text> : null}
          </View>
          <Text style={[styles.level, {color: teamColor}]}>{levelText}</Text>
        </View>

        {(update) ?
        <View style={styles.body}>
          <View style={styles.reporter}>
            <Image style={styles.cap} source={require('./assets/cap.png')} />
            <Text style={styles.reporterText}>
              {this.getUpdatedAtText(update)}
            </Text>
          </View>
          <View style={styles.thumbsContainer}>
            <TouchableHighlight onPress={this.vote.bind(this, update, true)} underlayColor={'#E5E5E5'}>
              <Image style={styles.thumb} source={require('./assets/thumbs_up.png')} />
            </TouchableHighlight>
            <Text style={styles.karma}>{this.state.score}</Text>
            <TouchableHighlight onPress={this.vote.bind(this, update, false)} underlayColor={'#E5E5E5'}>
              <Image style={styles.thumb} source={require('./assets/thumbs_down.png')} />
            </TouchableHighlight>
          </View>
        </View>
        : null}

        <Button onPress={() => {this.props.onUpdateGymClicked(_id)}}
          underlayColor='#C53434' style={styles.reportButton}>
          <Text style={styles.reportButtonText}>UPDATE GYM STATUS</Text>
        </Button>
      </View>
    </View>
  }

}

const styles = StyleSheet.create({
  container: {
    margin: 12,
    borderRadius: 4,
  },
  content: {
    justifyContent: 'center',
    padding: 16,
    backgroundColor: 'white',
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
  },
  triangleContainer: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  triangle: {
    width: 16,
    height: 16,
    marginBottom: -6,
  },
  line: {
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    height: 6
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  namePrestigeContainer: {
    paddingLeft: 16,
    paddingRight: 16,
    justifyContent: 'center'
  },
  teamIcon: {
    width: 50,
    height: 50
  },
  name: {
    color: '#4F4F4F',
    fontSize: 16,
    fontWeight: 'bold',
    marginLeft: 16,
    marginRight: 16,
    marginBottom: 4
  },
  prestige: {
    marginLeft: 16,
    fontSize: 14,
  },
  level: {
    fontSize: 40,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  body: {
    marginTop: 12,
    marginBottom: 12,
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  reporter: {
    flex: 1,
    marginRight: 24,
    flexDirection: 'row'
  },
  reporterText: {
    marginLeft: 16,
    opacity: 0.6,
    fontSize: 11,
    fontWeight: 'bold'
  },
  cap: {
    width: 20,
    height: 14
  },
  thumbsContainer: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  thumb: {
    width: 16,
    height: 16,
    opacity: 0.3
  },
  karma: {
    marginBottom: 4,
    marginTop: 4,
    opacity: 0.6,
    fontSize: 14,
    fontWeight: 'bold',
    textAlign: 'center'
  },
  reportButton: {
    backgroundColor: '#E53A3A',
    marginBottom: 0
  },
  reportButtonText: {
    fontWeight: 'bold',
    color: 'white'
  }
});
