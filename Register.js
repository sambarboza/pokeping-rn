import React, { Component } from 'react';
import {
  ActivityIndicator,
  Alert,
  Animated,
  AsyncStorage,
  Dimensions,
  Easing,
  Image,
  Modal,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';
import Button from './Button';
import TeamButton from './TeamButton';
var Fabric = require('react-native-fabric');
var { Answers, Crashlytics } = Fabric;

export default class Register extends Component {

  constructor(props) {
    super(props);
    this.state = {
      nickname: '',
      nicknameIsValid: false,
      selectedTeam: null,
      pageChanged: false,
      loading: false,
    }
    this.slideValue = new Animated.Value(0)
  }

  componentDidMount() {
    Answers.logContentView('Register', 'Screen', '')
  }

  onNextPressed() {
    Answers.logCustom('Register Next Page Clicked')
    const nickname = this.state.nickname
    if (nickname.length > 3) {
      this.setState({nicknameIsValid: true, pageChanged: true})
    } else {
      Answers.logCustom('Register - Nickame Invalid', {length: nickname.length})
      Alert.alert('Nickame should contain at least 3 characters')
    }
  }

  selectTeam(team, imageStyle, bgColor) {
    Answers.logCustom('Register - Team Selected', {team})
    this.setState({selectedTeam: team})
  }

  slideIn() {
    this.slideValue.setValue(0)
    Animated.timing(
      this.slideValue,
      {
        toValue: 1,
        duration: 400,
        easing: Easing.easeCubicOut
      }
    ).start()
  }

  register() {
    let self = this
    self.setState({loading: true, pageChanged: false})
    const user = {
      digitsId: this.props.userId,
      nickname: this.state.nickname,
      team: this.state.selectedTeam
    }
    Answers.logCustom('Register Request Sent', user)
    fetch('http://104.131.15.66/user', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(user)
    }).then((response) => response.json()).then((res) => {
      self.setState({loading: false})
      try {
        Answers.logCustom('Register Request Success')
        AsyncStorage.setItem('userId', res.user._id, () => {
          Answers.logSignUp('Digits', true, {
            nickname: user.nickname,
            team: user.team,
            userId: res.user._id
          })
          Crashlytics.setUserName(user.nickname)
          Crashlytics.setUserIdentifier(res.user._id)
          Crashlytics.setString('team', user.team+'')
          Alert.alert('Success', 'Welcome!', [
            {text: 'Ok', onPress: () => {
              self.props.close()
            }},
          ])
        })
      } catch (e) {
        Answers.logCustom('Register Request Error', {e})
        Alert.alert('Err', JSON.stringify(e))
      }
    }).catch((err) => {
      Answers.logCustom('Register Request Error', {err})
      self.setState({loading: false})
      alert(JSON.stringify(err))
      Alert.alert('Error', 'An error ocurred while trying to register')
    })
  }

  render() {
    if (this.state.selectedTeam === null) {
      this.slideIn()
    }
    const {height, width} = Dimensions.get('window')
    const teamMargin = 14
    const imageSize = ((width - 60) / 3)
    const slideIn = this.slideValue.interpolate({
      inputRange: [0, 1],
      outputRange: [200, 0]
    })
    const imageStyle = {width: imageSize, height: imageSize}

    const page1 = <View style={styles.container}>
      <Text style={styles.title}>Welcome!</Text>
      <Text style={styles.subtitle}>What is your in-game nickname?</Text>
      <TextInput style={styles.input}
        onChangeText={(nickname) => this.setState({nickname})}
        value={this.state.nickname}/>
      <Button onPress={this.onNextPressed.bind(this)}>
        <Text style={[styles.red, {fontWeight: '500'}]}>NEXT</Text>
      </Button>
    </View>

    const page2content =
    <View>
      <Text style={styles.title}>And,</Text>
      <Text style={styles.subtitle}>What is your team?</Text>
      <View style={styles.teamsContainer}>
        <TeamButton onPress={this.selectTeam.bind(this, 0)}
          selected={this.state.selectedTeam === 0}
          team={0} size={imageSize} style={{marginRight: teamMargin}} />
        <TeamButton onPress={this.selectTeam.bind(this, 1)}
          selected={this.state.selectedTeam == 1}
          team={1} size={imageSize} style={{marginRight: teamMargin}} />
        <TeamButton onPress={this.selectTeam.bind(this, 2)}
          selected={this.state.selectedTeam == 2}
          team={2} size={imageSize} />
      </View>
      <Button onPress={this.register.bind(this)}>
        <Text style={[styles.red, {fontWeight: '500'}]}>GO CATCH THEM!</Text>
      </Button>
      <ActivityIndicator
        animating={this.state.loading}
        style={[{height: 80}]}
        size="large"/>
    </View>

    const page2 = <View style={styles.container}>
      <Animated.View style={[{transform: [
          {translateX: slideIn}
        ]}]}>
        {page2content}
      </Animated.View>
    </View>

    return this.state.nicknameIsValid ? page2 : page1
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#E53A3A'
  },
  title: {
    color: 'white',
    fontSize: 48,
    fontWeight: 'bold',
  },
  subtitle: {
    color: 'white',
    fontSize: 24,
    fontWeight: '100',
    marginBottom: 60
  },
  input: {
    fontStyle: 'italic',
    color: 'white',
    height: 40,
    padding: 10,
    backgroundColor: 'rgba(255,255,255,0.4)',
    borderRadius: 3
  },
  red: {
    color: '#E53A3A'
  },
  teamsContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderRadius: 4
  }
});
