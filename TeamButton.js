import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  TouchableHighlight,
  View
} from 'react-native';

const valor = require('./assets/ic_valor.png')
const mystic = require('./assets/ic_mystic.png')
const instinct = require('./assets/ic_instinct.png')

export default class TeamButton extends Component {

  render() {
    const images = [valor, mystic, instinct]
    const colors = ['#F98A84', '#82BBF6', '#FFE981']

    const selectedStyle = (this.props.selected) ? {backgroundColor: colors[this.props.team]} : {}

    return <TouchableHighlight
              onPress={this.props.onPress}
              style={[styles.teamButton, selectedStyle, this.props.style]}
              underlayColor={colors[this.props.team]}>
        <Image source={images[this.props.team]} style={[styles.imageStyle, {height: this.props.size * 0.7}]} />
      </TouchableHighlight>
  }

}

const styles = StyleSheet.create({
  teamButton: {
    backgroundColor: 'white',
    padding: 12,
    borderRadius: 6,
    flexShrink: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    flexWrap: 'wrap',
  },
  imageStyle: {
    resizeMode: 'contain'
  }
})
