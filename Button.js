import React, { Component } from 'react';
import {
  TouchableHighlight,
  StyleSheet,
  View
} from 'react-native';
export default class Button extends Component {

  render () {
    return (
      <TouchableHighlight onPress={this.props.onPress}
        underlayColor={(this.props.underlayColor) ? this.props.underlayColor : '#E5E5E5'}
        style={[styles.buttonContainer, this.props.style]}>
        <View style={styles.button}>
          {this.props.children}
        </View>
      </TouchableHighlight>
    )
  }

}


const styles = StyleSheet.create({
  buttonContainer: {
    flexDirection:'row',
    padding: 14,
    marginBottom: 16,
    marginTop: 16,
    backgroundColor: 'white',
    borderRadius: 3,
  },
  button: {
    flex: 1,
    alignSelf: 'stretch',
    justifyContent: 'center',
    alignItems: 'center',
  }
});
