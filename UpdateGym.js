import React, { Component } from 'react';
import {
  ActivityIndicator,
  Alert,
  Animated,
  AsyncStorage,
  Dimensions,
  Easing,
  Image,
  Modal,
  Slider,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View
} from 'react-native';
import Button from './Button';
import TeamButton from './TeamButton';
var Fabric = require('react-native-fabric');
var { Answers, Crashlytics } = Fabric;
import { AdMobBanner } from 'react-native-admob'

export default class UpdateGym extends Component {
  totalPrestige = [0, 2, 4, 8, 12, 16, 20, 30, 40, 50, 52];

  constructor(props) {
    super(props);
    this.state = {
      level: 1,
      prestige: 0,
      team: null,
      loading: false,
      selectedTeam: null
    }
  }

  componentDidMount() {
    Answers.logContentView('Update Gym', 'Screen', '')
  }

  selectTeam(team, imageStyle, bgColor) {
    Answers.logCustom('Update Gym - Team Selected', {team})
    this.setState({selectedTeam: team})
  }

  getMinAndMaxPrestige() {
    const level = this.state.level
    const minPrestige = this.totalPrestige[level-1] * 1000
    const maxPrestige = this.totalPrestige[level] * 1000
    return {minPrestige, maxPrestige}
  }

  getPrestigeValue() {
    const { prestige, level } = this.state
    const maxAndMinPrestige = this.getMinAndMaxPrestige()
    if (prestige >= maxAndMinPrestige.minPrestige && prestige <= maxAndMinPrestige.maxPrestige) {
      return prestige
    } else {
      return this.totalPrestige[level-1] * 1000
    }
  }

  onUpdateGymClicked() {
    const update = {
      team: this.state.selectedTeam,
      level: this.state.level,
      prestige: this.state.prestige,
      user: this.props.userId
    }

    Answers.logCustom('Update Gym', {update})
    fetch(`http://104.131.15.66/gyms/update/${this.props.gymId}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(update)
    }).then((res) => {
      if (res.ok) {
        Answers.logCustom('Update Gym Success')
        console.log('Gym Update Successfully')
        this.props.close()
      } else {
        Answers.logCustom('Update Gym Error', {res})
        Alert.alert('Error')
      }
    }).catch((error) => {
      Answers.logCustom('Update Gym Error', {error})
      console.log('Error', error);
    })
  }

  render() {
    title = 'Update Gym';
    const { height, width } = Dimensions.get('window')
    const teamMargin = 14
    const imageSize = ((width - 60) / 3)
    const imageStyle = {width: imageSize, height: imageSize}
    const { minPrestige, maxPrestige } = this.getMinAndMaxPrestige()
    const { level, prestige } = this.state

    return <View style={{flex: 1}}>
      <View style={styles.container}>
        <View>
          <View style={styles.teamsContainer}>
            <TeamButton onPress={this.selectTeam.bind(this, 0)}
              selected={this.state.selectedTeam === 0}
              team={0} size={imageSize} style={{marginRight: teamMargin}} />
            <TeamButton onPress={this.selectTeam.bind(this, 1)}
              selected={this.state.selectedTeam == 1}
              team={1} size={imageSize} style={{marginRight: teamMargin}} />
            <TeamButton onPress={this.selectTeam.bind(this, 2)}
              selected={this.state.selectedTeam == 2}
              team={2} size={imageSize} />
          </View>

          <Text style={styles.label}>
            Level: {this.state.level}
          </Text>
          <Slider minimumValue={1} maximumValue={10} step={1} value={level}
            onValueChange={(value) => this.setState({level: value, prestige: 0})} />

          <Text style={styles.label}>
            Prestige: {this.getPrestigeValue.bind(this)()}/{maxPrestige}
          </Text>
          <Slider minimumValue={minPrestige} maximumValue={maxPrestige} step={25}
            value={this.getPrestigeValue.bind(this)()}
            onValueChange={(value) => this.setState({prestige: value})} />

          <Button onPress={this.onUpdateGymClicked.bind(this)}
            underlayColor='#C53434' style={[styles.reportButton, {marginTop: 32}]}>
            <Text style={styles.reportButtonText}>UPDATE GYM STATUS</Text>
          </Button>

          <ActivityIndicator
            animating={this.state.loading}
            style={[{height: 80}]}
            size="large"/>
        </View>
      </View>
      <AdMobBanner
        adUnitID="ca-app-pub-8358930606418017/3216687687"
        testDeviceID="EMULATOR"
        didFailToReceiveAdWithError={this.bannerError} />
    </View>
  }

}

let red = '#E53A3A'
let lightGray = '#F2F2F2'
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    backgroundColor: lightGray
  },
  label: {
    marginTop: 16,
    fontSize: 18,
    marginBottom: 8,
    fontWeight: 'bold',
    color: red
  },
  red: {
    color: red
  },
  teamsContainer: {
    paddingTop: 32,
    marginBottom: 16,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
  },
  reportButton: {
    backgroundColor: '#E53A3A',
    marginBottom: 0
  },
  reportButtonText: {
    fontWeight: 'bold',
    color: 'white'
  }
});
