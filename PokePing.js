import React, { Component } from 'react';
import {
  Alert,
  ActivityIndicator,
  Button,
  Image,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Map from './Map'
import Register from './Register'

export default class PokePing extends Component {

  render() {
    return (
      <Map />
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  }
});
